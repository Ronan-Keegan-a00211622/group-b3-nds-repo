import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Button;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class ViewDeliveryPerson {

	private JFrame frame;
	private JTextField textField;
	private DeliveryPerson deliveryperson;
	ResultSet rs = null;

	/**
	 * Launch the application.
	 */
	public static void ViewDeliveryPersonScreen(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewDeliveryPerson window = new ViewDeliveryPerson(dbobj);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @param dbobj
	 */
	public ViewDeliveryPerson(DatabaseConnector dbobj) {
		initialize(dbobj);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(DatabaseConnector dbobj) {
		frame = new JFrame("View Delivery Person");
		frame.setBounds(100, 100, 508, 589);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		deliveryperson = new DeliveryPerson(dbobj);

		JLabel lblViewCustomers = new JLabel("View Delivery Person");
		lblViewCustomers.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblViewCustomers.setBounds(140, 11, 224, 31);
		frame.getContentPane().add(lblViewCustomers);

		textField = new JTextField();
		textField.setBounds(140, 68, 189, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel lblName = new JLabel("Delivery Person:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName.setBounds(10, 68, 118, 17);
		frame.getContentPane().add(lblName);

		JLabel lblName_1 = new JLabel("Name:");
		lblName_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName_1.setBounds(10, 172, 78, 17);
		frame.getContentPane().add(lblName_1);

		JLabel lblDetails = new JLabel("Details");
		lblDetails.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblDetails.setBounds(202, 111, 78, 31);
		frame.getContentPane().add(lblDetails);

		JLabel name_box = new JLabel("Search for delivery person...");
		name_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		name_box.setBounds(140, 175, 224, 14);
		frame.getContentPane().add(name_box);

		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAddress.setBounds(10, 270, 78, 17);
		frame.getContentPane().add(lblAddress);

		JTextArea address_box = new JTextArea();
		address_box.setEnabled(false);
		address_box.setLineWrap(true);
		address_box.setText("Search for delivery person...");
		address_box.setBounds(140, 268, 189, 60);
		frame.getContentPane().add(address_box);

		JLabel lblPhone = new JLabel("Phone:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPhone.setBounds(10, 349, 78, 17);
		frame.getContentPane().add(lblPhone);

		JLabel phoneNumber_box = new JLabel("Search for delivery person...");
		phoneNumber_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		phoneNumber_box.setBounds(140, 351, 224, 14);
		frame.getContentPane().add(phoneNumber_box);

		JLabel lblPublications = new JLabel("Username:");
		lblPublications.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPublications.setBounds(10, 218, 96, 17);
		frame.getContentPane().add(lblPublications);

		JLabel username_box = new JLabel("Search for delivery person...");
		username_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		username_box.setBounds(140, 221, 224, 14);
		frame.getContentPane().add(username_box);

		Button button_3 = new Button("<< Return");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					DeliveryChoose nw = new DeliveryChoose(dbobj);
					nw.NewScreen5(dbobj);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				try {
					frame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}

			}
		});
		button_3.setBounds(34, 461, 86, 22);
		frame.getContentPane().add(button_3);

		JLabel lblAssignedRegion = new JLabel("Assigned Region:");
		lblAssignedRegion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAssignedRegion.setBounds(10, 402, 118, 17);
		frame.getContentPane().add(lblAssignedRegion);

		JLabel assignedRegions_box = new JLabel("Search for delivery person...");
		assignedRegions_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		assignedRegions_box.setBounds(140, 405, 224, 14);
		frame.getContentPane().add(assignedRegions_box);

		Button button = new Button("Search");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (!textField.getText().equals("")) {
						rs = deliveryperson.retrieveDeliveryPerson(textField.getText());

						while (rs.next()) {
							name_box.setText(rs.getString("name"));
							username_box.setText(rs.getString("username"));
							phoneNumber_box.setText(rs.getString("phone_number"));
							address_box.setText(rs.getString("address"));
							assignedRegions_box.setText(rs.getString("assigned_regions"));

						}
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		button.setBounds(351, 68, 70, 22);
		frame.getContentPane().add(button);
	}
}
