import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Button;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class ViewPublication {

	private JFrame frame;
	private JTextField textField;
	ResultSet rs = null;
	private Publication publication;

	/**
	 * Launch the application.
	 */
	public static void ViewPublicationScreen(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewPublication window = new ViewPublication(dbobj);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @param dbobj
	 */
	public ViewPublication(DatabaseConnector dbobj) {
		initialize(dbobj);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(DatabaseConnector dbobj) {
		frame = new JFrame("View Publication");
		frame.setBounds(100, 100, 508, 439);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		publication = new Publication(dbobj);

		JLabel lblViewCustomers = new JLabel("View Publication");
		lblViewCustomers.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblViewCustomers.setBounds(140, 11, 224, 31);
		frame.getContentPane().add(lblViewCustomers);

		textField = new JTextField();
		textField.setBounds(140, 68, 189, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel lblName = new JLabel("Publication Name:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName.setBounds(10, 68, 118, 17);
		frame.getContentPane().add(lblName);

		JLabel lblName_1 = new JLabel("Name:");
		lblName_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName_1.setBounds(10, 160, 78, 17);
		frame.getContentPane().add(lblName_1);

		JLabel lblDetails = new JLabel("Details");
		lblDetails.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblDetails.setBounds(202, 111, 78, 31);
		frame.getContentPane().add(lblDetails);

		JLabel lblNewLabel = new JLabel("Search for Publication...");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel.setBounds(140, 162, 224, 14);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblPhone = new JLabel("Cost:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPhone.setBounds(10, 216, 78, 17);
		frame.getContentPane().add(lblPhone);

		JLabel lblFrequency = new JLabel("Frequency:");
		lblFrequency.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblFrequency.setBounds(10, 241, 78, 17);
		frame.getContentPane().add(lblFrequency);

		JLabel lblSeachForDeliveryFrequency = new JLabel("Search for Publication...");
		lblSeachForDeliveryFrequency.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSeachForDeliveryFrequency.setBounds(140, 243, 224, 14);
		frame.getContentPane().add(lblSeachForDeliveryFrequency);

		JLabel lblSeachForDelivery = new JLabel("Search for Publication...");
		lblSeachForDelivery.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSeachForDelivery.setBounds(140, 218, 224, 14);
		frame.getContentPane().add(lblSeachForDelivery);

		JLabel lblPublications = new JLabel("Publication Date: ");
		lblPublications.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPublications.setBounds(10, 188, 118, 17);
		frame.getContentPane().add(lblPublications);

		JLabel lblSeachForPublication = new JLabel("Search for Publication...");
		lblSeachForPublication.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSeachForPublication.setBounds(140, 190, 224, 14);
		frame.getContentPane().add(lblSeachForPublication);

		Button button_3 = new Button("Return");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					PublicationChoose nw = new PublicationChoose(dbobj);
					nw.PublicationchooseMenu(dbobj);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				try {
					frame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}

			}
		});
		button_3.setBounds(10, 322, 86, 22);
		frame.getContentPane().add(button_3);

		JLabel lblAssignedRegion = new JLabel("Genre:");
		lblAssignedRegion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAssignedRegion.setBounds(10, 269, 118, 17);
		frame.getContentPane().add(lblAssignedRegion);

		JLabel lblSeachForPublication_1 = new JLabel("Search for Publication...");
		lblSeachForPublication_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSeachForPublication_1.setBounds(140, 271, 224, 14);
		frame.getContentPane().add(lblSeachForPublication_1);

		Button button = new Button("Search");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (!textField.getText().equals("")) {
						rs = publication.viewPublication(textField.getText());

						while (rs.next()) {
							lblNewLabel.setText(rs.getString("publication_name"));
							lblSeachForPublication.setText(rs.getString("publication_date"));
							lblSeachForDelivery.setText("" + rs.getDouble("cost"));
							lblSeachForDeliveryFrequency.setText(rs.getString("publication_frequency"));
							lblSeachForPublication_1.setText(rs.getString("genre"));

							if (rs.isLast()) {
								rs.beforeFirst();
								break;
							}

						}
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		button.setBounds(351, 68, 70, 22);
		frame.getContentPane().add(button);
	}
}
