import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Button;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class EditCustomer {

	private JFrame frame;
	private JTextField textField;
	private Customer customer;
	private JTextField lastName_box;
	private JTextArea address_box;
	private JTextField phoneNumber_box;
	private JTextArea textArea;
	private JLabel lblDob;
	private JTextField d_O_B_box;
	private JTextField region_box;
	private JTextField deliveryFreq_box;
	private JTextField otherNames_box;
	private Customer[] cust;
	private Button button_1;
	private Button button_2;
	private Button button_3;
	ResultSet rs = null;
	int theId = 0;

	/**
	 * Launch the application.
	 */
	public static void EditCustomersScreen(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditCustomer window = new EditCustomer(dbobj);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @param dbobj
	 */
	public EditCustomer(DatabaseConnector dbobj) {
		initialize(dbobj);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(DatabaseConnector dbobj) {
		frame = new JFrame("Edit Customer");
		frame.setBounds(100, 100, 508, 695);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		customer = new Customer(dbobj);

		JLabel lblViewCustomers = new JLabel("Edit Customers");
		lblViewCustomers.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblViewCustomers.setBounds(164, 11, 165, 31);
		frame.getContentPane().add(lblViewCustomers);

		textField = new JTextField();
		textField.setBounds(105, 68, 224, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel lblName = new JLabel("Customer:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName.setBounds(10, 68, 78, 17);
		frame.getContentPane().add(lblName);

		Button button = new Button("Search");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (!textField.getText().equals("")) {
						rs = customer.viewCustomerDetails(textField.getText());

						while (rs.next()) {
							theId = Integer.parseInt(rs.getString("id"));
							lastName_box.setText(rs.getString("last_name"));
							otherNames_box.setText(rs.getString("other_names"));
							d_O_B_box.setText(rs.getString("date_of_birth"));
							address_box.setText(rs.getString("address"));
							region_box.setText(rs.getString("region"));
							phoneNumber_box.setText(rs.getString("phone_number"));
							deliveryFreq_box.setText(rs.getString("delivery_frequency"));
							textArea.setText(dbobj.retrieveCustomerSubscriptions(Integer.parseInt(rs.getString("id"))));

							if (rs.isClosed()) {
								break;
							}
						}
						rs = customer.viewCustomerDetails(textField.getText());

						// lblNewLabel.setText(cust[0].lastName + " " + cust[0].otherNames);
						// lblDob.setText(cust[0].dateOfBirth);
						// txtrSearchForCustomer.setText(cust[0].address);
						// label_2.setText(cust[0].region);
						// label.setText(cust[0].phoneNumber);
						// textArea.setText(cust[0].customerPublications);
						// label_3.setText(cust[0].deliveryFrequency);
						// if (cust.length > 1) {
						// button_2.setEnabled(true);
						// } else {
						// button_2.setEnabled(false);
						// }

					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		button.setBounds(351, 68, 70, 22);
		frame.getContentPane().add(button);

		JLabel lblName_1 = new JLabel("Last Name:");
		lblName_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName_1.setBounds(10, 154, 78, 17);
		frame.getContentPane().add(lblName_1);

		JLabel lblName_2 = new JLabel("Other Names:");
		lblName_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName_2.setBounds(10, 182, 96, 17);
		frame.getContentPane().add(lblName_2);

		JLabel lblDetails = new JLabel("Details");
		lblDetails.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblDetails.setBounds(202, 111, 78, 31);
		frame.getContentPane().add(lblDetails);

		lastName_box = new JTextField("Search for customer...");
		lastName_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lastName_box.setBounds(175, 153, 224, 20);
		frame.getContentPane().add(lastName_box);

		otherNames_box = new JTextField("Search for customer...");
		otherNames_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		otherNames_box.setBounds(175, 181, 224, 20);
		frame.getContentPane().add(otherNames_box);

		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAddress.setBounds(10, 297, 78, 17);
		frame.getContentPane().add(lblAddress);

		address_box = new JTextArea();
		address_box.setEnabled(true);
		address_box.setLineWrap(true);
		address_box.setText("Search for customer...");
		address_box.setBounds(175, 295, 189, 60);
		frame.getContentPane().add(address_box);

		JLabel lblPhone = new JLabel("Phone:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPhone.setBounds(10, 438, 78, 17);
		frame.getContentPane().add(lblPhone);

		phoneNumber_box = new JTextField("Search for customer...");
		phoneNumber_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		phoneNumber_box.setBounds(175, 440, 224, 20);
		frame.getContentPane().add(phoneNumber_box);

		textArea = new JTextArea();
		textArea.setText("Search for customer...");
		textArea.setLineWrap(true);
		textArea.setEnabled(false);
		textArea.setBounds(175, 465, 189, 81);
		frame.getContentPane().add(textArea);

		JLabel lblPublications = new JLabel("Publications:");
		lblPublications.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPublications.setBounds(10, 466, 96, 17);
		frame.getContentPane().add(lblPublications);

		lblDob = new JLabel("DOB:");
		lblDob.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDob.setBounds(10, 227, 78, 17);
		frame.getContentPane().add(lblDob);

		d_O_B_box = new JTextField("Search for customer...");
		d_O_B_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		d_O_B_box.setBounds(175, 229, 224, 20);
		frame.getContentPane().add(d_O_B_box);

		JLabel lblRegion = new JLabel("Region:");
		lblRegion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblRegion.setBounds(10, 381, 78, 17);
		frame.getContentPane().add(lblRegion);

		region_box = new JTextField("Search for customer...");
		region_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		region_box.setBounds(175, 383, 224, 20);
		frame.getContentPane().add(region_box);

		JLabel lblDeliveryFrequency = new JLabel("Delivery Frequency:");
		lblDeliveryFrequency.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDeliveryFrequency.setBounds(10, 571, 136, 17);
		frame.getContentPane().add(lblDeliveryFrequency);

		deliveryFreq_box = new JTextField("Search for customer...");
		deliveryFreq_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		deliveryFreq_box.setBounds(175, 573, 224, 20);
		frame.getContentPane().add(deliveryFreq_box);

		button_1 = new Button("Edit");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					String r = "";
					// Custom button text
					Object[] options = { "Yes, Edit", "No, Cancel", };
					int n = JOptionPane.showOptionDialog(frame, "Are you sure that you want to Edit the Customer ",
							"Warning", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
							options[1]);

					if (n == 0) {
						rs.beforeFirst();
						while (rs.next()) {
							r = customer.updateCustomer(rs.getInt("id"), lastName_box.getText(),
									otherNames_box.getText(), d_O_B_box.getText(), address_box.getText(),
									region_box.getText(), deliveryFreq_box.getText(), phoneNumber_box.getText());
							JOptionPane.showMessageDialog(null, r);
							if (r == "Customer details successfully updated.") {
								textField.setText("");
								lastName_box.setText("");
								otherNames_box.setText("");
								d_O_B_box.setText("");
								address_box.setText("");
								region_box.setText("");
								deliveryFreq_box.setText("");
								phoneNumber_box.setText("");
								break;
							} else if (!r.equals("")) {
								break;
							}

						}
					} else if (n == 1) {
						System.out.println("Cancel");
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}

			}
		});
		button_1.setBounds(243, 610, 86, 22);
		frame.getContentPane().add(button_1);

		/*
		 * button_2 = new Button("Delete"); button_2.addActionListener(new
		 * ActionListener() { public void actionPerformed(ActionEvent e) {
		 * 
		 * if (textField.getText() == "") {
		 * JOptionPane.showMessageDialog(null,"No Customer Selected"); } else {
		 * 
		 * try { String r = ""; //Custom button text Object[] options = {"Yes, Delete",
		 * "No, Cancel", }; int n = JOptionPane.showOptionDialog(frame,
		 * "Are you sure that you want to Delete the Customer ", "Warning",
		 * JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
		 * options[1]);
		 * 
		 * if (n == 0) { rs.beforeFirst(); while (rs.next()) { r =
		 * customer.deletecustomer(rs.getInt("id")); if (rs.isClosed()) { rs =
		 * deliveryperson.retrieveDeliveryPerson(textField.getText()); rs.beforeFirst();
		 * break; } } } else if (n == 1) {
		 * 
		 * } }catch(Exception e1) { e1.printStackTrace(); } }
		 * 
		 * } }); button_2.setBounds(335, 610, 86, 22);
		 * frame.getContentPane().add(button_2);
		 */

		button_3 = new Button("Return");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					CustChoose nw = new CustChoose(dbobj);
					nw.NewScreen4(dbobj);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				try {
					frame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		button_3.setBounds(60, 610, 86, 22);
		frame.getContentPane().add(button_3);
	}
}
