import java.sql.ResultSet;

import junit.framework.TestCase;

public class DeliveryPersonTest extends TestCase {

	// Test no. 1
	// Objective to test: To test Name for valid chars
	// Input(s): UserName = "Tommy12345"
	// Expected Output: False
	public void testvalidateName001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(false, testObject.validateName("Tommy12345"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 2
	// Objective to test: To test Name valid chars
	// Input(s): UserName = "Tommy"
	// Expected Output: True
	public void testvalidateName002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(true, testObject.validateName("Tommy"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 3
	// Objective to test: To test username for valid chars
	// Input(s): UserName = "Tommy12345"
	// Expected Output: True
	public void testvalidateUsername003() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(true, testObject.validateUsername("Tommy12345"));

		} catch (Exception e) {
			fail("Exception not Expected");
		}

	}

	// Test no. 4
	// Objective to test: To test username valid chars
	// Input(s): UserName = "Tommy"
	// Expected Output: False
	public void testvalidateUsername004() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(false, testObject.validateUsername("!%$&"));

		} catch (Exception e) {
			fail("Exception expected");
		}

	}

	// Test no. 5
	// Objective to test: To test password in range 0 to 3 chars
	// Input(s): Password = "abc"
	// Expected Output: False
	public void testvalidatePassword005() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(false, testObject.validatePassword("abc"));

		} catch (Exception e) {
			fail("Exception expected");
		}
	}

	// Test no. 6
	// Objective to test: To test password in range 100 to MAX chars
	// Input(s): Password = "abcdefghijklmnoprstuywvz"
	// Expected Output: False
	public void testvalidatePassword006() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(false, testObject.validatePassword("abcdefghijklmnoprstuywvz"));

		} catch (Exception e) {
			fail("Exception expected");
		}
	}

	// Test no. 7
	// Objective to test: To test password in range 4 to 9 chars
	// Input(s): Password = "abcdefgh"
	// Expected Output: True
	public void testvalidatePassword007() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(true, testObject.validatePassword("abcdefgh"));

		} catch (Exception e) {
			fail("Exception not expected");
		}
	}

	// Test no. 8
	// Objective to test: To test phone number invalid chars
	// Input(s): Password = "abcdefghijklmnoprstuywvz"
	// Expected Output: False
	public void testvalidatePhoneNumber008() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(false, testObject.validatePhoneNumber("abcdefghijklm"));

		} catch (Exception e) {
			fail("Exception expected");
		}
	}

	// Test no. 9
	// Objective to test: To test phone number valid chars
	// Input(s): Password = "abcdefgh"
	// Expected Output: True
	public void testvalidatePhoneNumber009() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(true, testObject.validatePhoneNumber("0891234567"));

		} catch (Exception e) {
			fail("Exception not expected");
		}
	}

	// Test no. 10
	// Objective to test: If the Region is not empty
	// Input(s): RegionID = "15"
	// Expected Output: true
	public void testvalidateAssignedRegion010() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(true, testObject.validateAssignedRegion("ATH 1, ATH 2"));

		} catch (Exception e) {
			fail("Exception Expected");
		}
	}

	// Test no. 11
	// Objective to test: If the Region is empty
	// Input(s): RegionID = "0"
	// Expected Output: false
	public void testvalidateAssignedRegion011() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(false, testObject.validateAssignedRegion(""));

		} catch (Exception e) {
			fail("Exception not Expected");
		}
	}
	
	public void testvalidatecreateDeliveryPerson012() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("ok", testObject.createDeliveryPerson("John", "John1234", "12qwert4%", "0891234567","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateDeliveryPerson013() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Name", testObject.createDeliveryPerson("123�John$", "John1234", "12qwert4%", "0891234567","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateDeliveryPerson014() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Username", testObject.createDeliveryPerson("John", "$$$$t%%%%", "12qwert4%", "0891234567","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateDeliveryPerson015() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Password", testObject.createDeliveryPerson("John", "John1234", "abc", "0891234567","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateDeliveryPerson016() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Phone Number", testObject.createDeliveryPerson("John", "John1234", "12qwert4%", "testPhone","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateDeliveryPerson017() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Address", testObject.createDeliveryPerson("John", "John1234", "12qwert4%", "0891234567","","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateDeliveryPerson018() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Region(s)", testObject.createDeliveryPerson("John", "John1234", "12qwert4%", "0891234567","Address",""));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	// Test no. 19
	// Objective to test: To test valid value deliveryPerson
	// Input(s): deliveryPerson = "abc"
	// Expected Output: new DeliveryPerson()
	public void testviewDeliveryPersonDetails019() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals(true, testObject.viewDeliveryPersonDetails("abc") instanceof ResultSet);
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	// Test no. 20
	// Objective to test: To test invalid value deliveryPerson
	// Input(s): deliveryPerson = ""
	// Expected Output: null
	public void testviewDeliveryPersonDetails020() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals(true, testObject.viewDeliveryPersonDetails("") == null);
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 21
	// Objective to test: When id <= 0
	// Input(s): id = 0
	// Expected Output: false
	public void testvalidateId021() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals(false, testObject.validateId(0));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not Expected");
		}
	}
	
	// Test no. 22
	// Objective to test: When id > Integer.MAX_VALUE
	// Input(s): id = (Integer.MAX_VALUE + 1)
	// Expected Output: false
	public void testvalidateId022() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals(false, testObject.validateId(Integer.MAX_VALUE + 1));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not Expected");
		}
	}
	
	// Test no. 23
	// Objective to test: When id > 0 and id <= Integer.MAX_VALUE
	// Input(s): id = 1
	// Expected Output: true
	public void testvalidateId023() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals(true, testObject.validateId(1));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not Expected");
		}
	}
	
	
	// Test no. 24
	// Objective to test: When id > 0 and id <= Integer.MAX_VALUE
	// Input(s): id = Integer.MAX_VALUE
	// Expected Output: true
	public void testvalidateId024() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals(true, testObject.validateId(Integer.MAX_VALUE));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not Expected");
		}
	}
	
	// Test no. 25
	// Objective to test: To test invalid id value when updating a delivery person
	// Input(s): id = 0, name = "John", username = "John1234", password = "12qwert4%", phoneNumber = "0891234567", address = "Address", region = "ATH 1, ATH 2"
	// Expected Output: "Incorrect ID"
	public void testupdateDeliveryPerson025() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect ID", testObject.updateDeliveryPerson(0, "John", "John1234", "12qwert4%", "0891234567","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	// Test no. 26
	// Objective to test: To test invalid name value when updating a delivery person
	// Input(s): id = 1, name = "123�John$", username = "John1234", password = "12qwert4%", phoneNumber = "0891234567", address = "Address", region = "ATH 1, ATH 2"
	// Expected Output: "Incorrect Name"
	public void testupdateDeliveryPerson026() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Name", testObject.updateDeliveryPerson(1, "123�John$", "John1234", "12qwert4%", "0891234567","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	// Test no. 27
	// Objective to test: To test invalid username value when updating a delivery person
	// Input(s): id = 1, name = "John", username = "$$$$t%%%%", password = "12qwert4%", phoneNumber = "0891234567", address = "Address", region = "ATH 1, ATH 2"
	// Expected Output: "Incorrect Username"
	public void testupdateDeliveryPerson027() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Username", testObject.updateDeliveryPerson(1, "John", "$$$$t%%%%", "12qwert4%", "0891234567","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	// Test no. 28
	// Objective to test: To test invalid password value when updating a delivery person
	// Input(s): id = 1, name = "John", username = "John1234", password = "abc", phoneNumber = "0891234567", address = "Address", region = "ATH 1, ATH 2"
	// Expected Output: "Incorrect Password"
	public void testupdateDeliveryPerson028() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Password", testObject.updateDeliveryPerson(1, "John", "John1234", "abc", "0891234567","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	// Test no. 29
	// Objective to test: To test invalid phoneNumber value when updating a delivery person
	// Input(s): id = 1, name = "John", username = "John1234", password = "12qwert4%", phoneNumber = "testPhone", address = "Address", region = "ATH 1, ATH 2"
	// Expected Output: "Incorrect Phone Number"
	public void testupdateDeliveryPerson029() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Phone Number", testObject.updateDeliveryPerson(1, "John", "John1234", "12qwert4%", "testPhone","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	// Test no. 30
	// Objective to test: To test invalid phoneNumber value when updating a delivery person
	// Input(s): id = 1, name = "John", username = "John1234", password = "12qwert4%", phoneNumber = "0891234567", address = "", region = "ATH 1, ATH 2"
	// Expected Output: "Incorrect Address"
	public void testupdateDeliveryPerson030() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Address", testObject.updateDeliveryPerson(1, "John", "John1234", "12qwert4%", "0891234567","","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	// Test no. 31
	// Objective to test: To test invalid phoneNumber value when updating a delivery person
	// Input(s): id = 1, name = "John", username = "John1234", password = "12qwert4%", phoneNumber = "0891234567", address = "Address", region = ""
	// Expected Output: "Incorrect Region(s)"
	public void testupdateDeliveryPerson031() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Region(s)", testObject.updateDeliveryPerson(1, "John", "John1234", "12qwert4%", "0891234567","Address",""));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	//Test no. 32
	// Objective to test: To test valid DeliveryPerson address
	// Input(s): deliveryPerson = "abc"
	// Expected Output: ResultSet object
	public void testviewDeliveryPersonDetails032() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals(true, testObject.viewDeliveryPersonDetails("abc") instanceof ResultSet);
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}

	//Test no. 33
	// Objective to test: To test invalid DeliveryPerson address
	// Input(s): deliveryPerson = ""
	// Expected Output: null
	public void testviewDeliveryPersonDetails033() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals(true, testObject.viewDeliveryPersonDetails("") == null);
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	//Test no. 34
	// Objective to test: To test invalid DeliveryPerson id
	// Input(s): id = 0
	// Expected Output: "Incorrect ID"
	public void testviewDeliveryPersonDetails034() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals("Incorrect ID.", testObject.deleteDeliveryPerson(0));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	//Test no. 35
	// Objective to test: To test invalid DeliveryPerson id
	// Input(s): id = Integer.MAX_VALUE + 1
	// Expected Output: "Incorrect ID"
	public void testviewDeliveryPersonDetails035() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals("Incorrect ID.", testObject.deleteDeliveryPerson(Integer.MAX_VALUE + 1));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	//Test no. 36
	// Objective to test: To test invalid DeliveryPerson id
	// Input(s): id = 1
	// Expected Output: Delivery Person has been removed.
	public void testviewDeliveryPersonDetails036() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals("Delivery Person has been removed.", testObject.deleteDeliveryPerson(1));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 37
	// Objective to test: To test valid value deliveryPerson
	// Input(s): deliveryPerson = "abc"
	// Expected Output: instance of ResultSet
	public void testviewDeliveryPerson037() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals(true, testObject.retrieveDeliveryPerson("abc") instanceof ResultSet);
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	// Test no. 38
	// Objective to test: To test invalid value deliveryPerson
	// Input(s): deliveryPerson = ""
	// Expected Output: true
	public void testviewDeliveryPerson038() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals(true, testObject.retrieveDeliveryPerson("") == null);
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
}



