import java.sql.ResultSet;

import junit.framework.TestCase;

public class PublicationTest extends TestCase {

	// Test no. 1
	// Objective to test: To test PublicationName for invalid chars
	// Input(s): PublicationName = "~#Irish Times"
	// Expected Output: False
	public void testvalidatePublicationName001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(false, testObject.validatePublicationName("~#IrishTimes"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 2
	// Objective to test: To test PublicationName for valid chars
	// Input(s): PublicationName = "Irish Times"
	// Expected Output: True
	public void testvalidatePublicationName002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(true, testObject.validatePublicationName("IrishTimes"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 3
	// Objective to test: To test PublicationDate for valid chars
	// Input(s): publicationDate = "11-22-96"
	// Expected Output: True

	public void testvalidatePublicationDate003() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(true, testObject.validatePublicationDate("11-22-96"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 4
	// Objective to test: To test PublicationDate for valid chars
	// Input(s): PublicationDate = "112-22-9a"
	// Expected Output: False

	public void testvalidatePublicationDate004() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(false, testObject.validatePublicationDate("112-22-9a"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 5
	// Objective to test: To test publication cost for invalid number
	// Input(s): Pub Cost = "-2"
	// Expected Output: False

	public void testvalidatePublicationCost005() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(false, testObject.validatePublicationCost(-2));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 6
	// Objective to test: To test publication cost for valid number
	// Input(s): PublicationCost = "22"
	// Expected Output: True

	public void testvalidatePublicationCost006() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(true, testObject.validatePublicationCost(22));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 7
	// Objective to test: To test invalid publication frequency
	// Input(s): PublicationFrequency = ""
	// Expected Output: False

	public void testvalidatePublicationFrequency007() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(false, testObject.validatePublicationFrequency(""));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 8
	// Objective to test: To test valid publication frequency
	// Input(s): PublicationFrequency = "weekly"
	// Expected Output: True

	public void testvalidatePublicationFrequency008() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(true, testObject.validatePublicationFrequency("weekly"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 9
	// Objective to test: To test PublicationName for invalid chars
	// Input(s): PublicationGenere = "#$%Spor%t"
	// Expected Output: False
	public void testvalidatePublicationGenere009() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(false, testObject.validatePublicationGenere("#$%Spor%t"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 10
	// Objective to test: To test PublicationName for valid chars
	// Input(s): PublicationGenere = "Sport"
	// Expected Output: True
	public void testvalidatePublicationGenere010() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(true, testObject.validatePublicationGenere("Sport"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}
	
	// Test no. 11
	// Objective to test: To test PublicationName for valid chars
	// Input(s): PublicationGenere = "Irish Times", "22-12-18", 5.5, "weekly", "Sport"
	// Expected Output: "ok"
	public void testcreatePublication011() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			
			assertEquals("ok", testObject.createPublication("Irish Times", "22-12-18", 5.5, "weekly", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	// Test no. 12
	// Objective to test: To test PublicationName for invalid chars
	// Input(s): PublicationGenere = "Irish Times", "22-12-18", 5.5, "weekly", "Sport"
	// Expected Output: "Incorrect Publication Name"
	public void testcreatePublication012() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("Incorrect Publication Name",
					testObject.createPublication("#Iri~sh Times", "22-12-18", 5.5, "weekly", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}

	}
	// Test no. 13
	// Objective to test: To test PublicationName for invalid Date
	// Input(s): PublicationGenere = "Irish Times", "22-12-18", 5.5, "weekly", "Sport"
	// Expected Output: "Incorrect Publication Date"
	public void testcreatePublication013() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("Incorrect Publication Date",
					testObject.createPublication("Irish Times", "222-12-1a", 5.5, "weekly", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}

	}
	// Test no. 14
	// Objective to test: To test PublicationCost for invalid cost
	// Input(s): PublicationGenere = "Irish Times", "22-12-18", 5.5, "weekly", "Sport"
	// Expected Output: "Incorrect Publication Cost"
	public void testcreatePublication014() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("Incorrect Publication Cost",
					testObject.createPublication("Irish Times", "22-12-18", 0, "weekly", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}

	}
	// Test no. 15
	// Objective to test: To test PublicationName for valid chars
	// Input(s): PublicatioName = "Irish Times", "22-12-18", 5.5, "weekly", "Sport"
	// Expected Output: "Incorrect Publication Frequency"
	public void testcreatePublication015() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("Incorrect Publication Frequency",
					testObject.createPublication("Irish Times", "22-12-18", 5.5, "", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}
	}
	// Test no. 16
	// Objective to test: To test PublicationName for valid chars
	// Input(s): PublicationGenere = "Irish Times", "22-12-18", 5.5, "weekly", "Sport"
	// Expected Output: True
	public void testcreatePublication016() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObj = new Publication(db);
			assertEquals("Incorrect Publication Genere",
					testObj.createPublication("Irish Times", "22-12-18", 5.5, "weekly", "S#po~rt"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}

	}

	// Test no. 17
	// Objective to test: To test invalid id value when updating a publication
	// Input(s): id = 0, publicationName = "Irish Times", publicationDate = "22-12-18", cost = 5.50, publicationFrequency = "weekly", genre = "Sport"
	// Expected Output: "Incorrect ID"
	public void testupdatePublication017() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("Incorrect ID", testObject.updatePublication(0, "Irish Times", "22-12-18", 5.50, "weekly", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}

	// Test no. 18
	// Objective to test: To test invalid publicationName value when updating a publication
	// Input(s): id = 1, publicationName = "#Iri~sh Times", publicationDate = "22-12-18", cost = 5.50, publicationFrequency = "weekly", genre = "Sport"
	// Expected Output: "Incorrect Publication Name"
	public void testupdatePublication018() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("Incorrect Publication Name", testObject.updatePublication(1, "#Iri~sh Times", "22-12-18", 5.5, "weekly", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}

	}

	// Test no. 19
	// Objective to test: To test invalid publicationDate value when updating a publication
	// Input(s): id = 1, publicationName = "Irish Times", publicationDate = "222-12-1a", cost = 5.50, publicationFrequency = "weekly", genre = "Sport"
	// Expected Output: "Incorrect Publication Date"
	public void testupdatePublication019() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("Incorrect Publication Date",
					testObject.updatePublication(1, "Irish Times", "222-12-1a", 5.50, "weekly", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}

	}

	// Test no. 20
	// Objective to test: To test invalid cost value when updating a publication
	// Input(s): id = 1, publicationName = "Irish Times", publicationDate = "22-12-18", cost = 0.00, publicationFrequency = "weekly", genre = "Sport"
	// Expected Output: "Incorrect Publication Cost"
	public void testupdatePublication020() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("Incorrect Publication Cost", testObject.updatePublication(1, "Irish Times", "22-12-18", 0.00, "weekly", "Sport"));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}

	}

	// Test no. 21
	// Objective to test: To test invalid publicationFrequency value when updating a publication
	// Input(s): id = 1, publicationName = "Irish Times", publicationDate = "22-12-18", cost = 5.50, publicationFrequency = "", genre = "Sport"
	// Expected Output: "Incorrect Publication Frequency"
	public void testupdatePublication021() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("Incorrect Publication Frequency", testObject.updatePublication(1, "Irish Times", "22-12-18", 5.50, "", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}
	}
	
	// Test no. 22
	// Objective to test: To test invalid publicationDate value when updating a publication
	// Input(s): id = 1, publicationName = "Irish Times", publicationDate = "22-12-18", cost = 5.50, publicationFrequency = "weekly", genre = "S#po~rt"
	// Expected Output: "Incorrect Publication Genre"
	public void testupdatePublication022() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObj = new Publication(db);
			assertEquals("Incorrect Publication Genre", testObj.updatePublication(1, "Irish Times", "22-12-18", 5.50, "weekly", "S#po~rt"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}
	}
	
	// Test no. 23
	// Objective to test: To test valid values when updating a publication
	// Input(s): id = 1, publicationName = "Irish Times", publicationDate = "22-12-18", cost = 5.50, publicationFrequency = "yearly", genre = "Sport"
	// Expected Output: "Incorrect Publication Genre"
	public void testupdatePublication023() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("ok", testObject.updatePublication(1, "Irish Times", "22-12-18", 5.50, "weekly", "Sport"));
	
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}
	
		
	//Test no. 24
	// Objective to test: To test invalid Publication id
	// Input(s): id = 0
	// Expected Output: "Publication has been removed."
	public void testviewPublicationDetails024() {
			try {
				DatabaseConnector db = new DatabaseConnector();
				Publication testObject = new Publication(db);
				
				assertEquals("Incorrect ID.", testObject.deletePublication(0));
			} catch(Exception ex) {
				fail("Exception not expected");
				ex.printStackTrace();
			}
		}
		
	//Test no. 25
	// Objective to test: To test invalid Publication id
	// Input(s): id = Integer.MAX_VALUE + 1
	// Expected Output: "Incorrect ID"
	public void testviewPublicationDetails025() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			
			assertEquals("Incorrect ID.", testObject.deletePublication(Integer.MAX_VALUE + 1));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
		
	//Test no. 26
	// Objective to test: To test invalid Publication id
	// Input(s): id = 1
	// Expected Output: Publication has been removed.
	public void testviewPublicationDetails026() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			
			assertEquals("Publication has been removed.", testObject.deletePublication(1));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 27
	// Objective to test: To test valid value publicationName
	// Input(s): publicationName = "abc"
	// Expected Output: instance of ResultSet
	public void testviewPublication027() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			
			assertEquals(true, testObject.viewPublication("abc") instanceof ResultSet);
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 28
	// Objective to test: To test invalid value publicationName
	// Input(s): publicationName = ""
	// Expected Output: true
	public void testviewPublication028() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			
			assertEquals(true, testObject.viewPublication("") == null);
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
}
