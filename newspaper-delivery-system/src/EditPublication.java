import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Button;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class EditPublication {

	private JFrame frame;
	private JTextField textField;
	ResultSet rs = null;
	private Publication publication;

	/**
	 * Launch the application.
	 */
	public static void EditPublicationScreen(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditPublication window = new EditPublication(dbobj);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @param dbobj
	 */
	public EditPublication(DatabaseConnector dbobj) {
		initialize(dbobj);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(DatabaseConnector dbobj) {
		frame = new JFrame("Edit Publication");
		frame.setBounds(100, 100, 508, 439);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		publication = new Publication(dbobj);

		JLabel lblViewCustomers = new JLabel("Edit Publication");
		lblViewCustomers.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblViewCustomers.setBounds(140, 11, 224, 31);
		frame.getContentPane().add(lblViewCustomers);

		textField = new JTextField();
		textField.setBounds(140, 68, 189, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel lblName = new JLabel("Publication Name:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName.setBounds(10, 68, 118, 17);
		frame.getContentPane().add(lblName);

		JLabel lblName_1 = new JLabel("Name:");
		lblName_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName_1.setBounds(10, 160, 78, 17);
		frame.getContentPane().add(lblName_1);

		JLabel lblDetails = new JLabel("Details");
		lblDetails.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblDetails.setBounds(202, 111, 78, 31);
		frame.getContentPane().add(lblDetails);

		JTextArea pubName_box = new JTextArea("Search for Publication...");
		pubName_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		pubName_box.setBounds(140, 162, 224, 20);
		frame.getContentPane().add(pubName_box);

		JLabel lblPhone = new JLabel("Cost:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPhone.setBounds(10, 216, 78, 17);
		frame.getContentPane().add(lblPhone);

		JLabel lblFrequency = new JLabel("Frequency:");
		lblFrequency.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblFrequency.setBounds(10, 241, 78, 17);
		frame.getContentPane().add(lblFrequency);

		JTextArea pubFreq_box = new JTextArea("Search for Publication...");
		pubFreq_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		pubFreq_box.setBounds(140, 243, 224, 20);
		frame.getContentPane().add(pubFreq_box);

		JTextArea cost_box = new JTextArea("Search for Publication...");
		cost_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cost_box.setBounds(140, 218, 224, 20);
		frame.getContentPane().add(cost_box);

		JLabel lblPublications = new JLabel("Publication Date: ");
		lblPublications.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPublications.setBounds(10, 188, 118, 17);
		frame.getContentPane().add(lblPublications);

		JTextArea pubDate_box = new JTextArea("Search for Publication...");
		pubDate_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		pubDate_box.setBounds(140, 190, 224, 20);
		frame.getContentPane().add(pubDate_box);

		Button button_3 = new Button("Return");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					PublicationChoose nw = new PublicationChoose(dbobj);
					nw.PublicationchooseMenu(dbobj);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				try {
					frame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}

			}
		});
		button_3.setBounds(10, 322, 86, 22);
		frame.getContentPane().add(button_3);

		JLabel lblAssignedRegion = new JLabel("Genre:");
		lblAssignedRegion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAssignedRegion.setBounds(10, 269, 118, 17);
		frame.getContentPane().add(lblAssignedRegion);

		JTextArea genre_box = new JTextArea("Search for Publication...");
		genre_box.setFont(new Font("Tahoma", Font.PLAIN, 13));
		genre_box.setBounds(140, 271, 224, 20);
		frame.getContentPane().add(genre_box);

		Button button_2 = new Button("Edit");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String r = "";
					// Custom button text
					Object[] options = { "Yes, Edit", "No, Cancel", };
					int n = JOptionPane.showOptionDialog(frame, "Are you sure that you want to Edit the Publication ",
							"Warning", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
							options[1]);

					if (n == 0) {
						rs.beforeFirst();
						while (rs.next()) {
							r = publication.updatePublication(rs.getInt("id"), pubName_box.getText(),
									pubDate_box.getText(), Double.parseDouble(cost_box.getText()),
									pubFreq_box.getText(), genre_box.getText());
							JOptionPane.showMessageDialog(null, r);
							if (r == "Publication has been sucessfully updated") {
								textField.setText("");
								pubName_box.setText("");
								pubDate_box.setText("");
								cost_box.setText("");
								pubFreq_box.setText("");
								genre_box.setText("");
								break;
							} else if (!r.equals("")) {
								break;
							}

						}
					} else if (n == 1) {

					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		button_2.setBounds(335, 322, 86, 22);
		frame.getContentPane().add(button_2);

		Button button = new Button("Search");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (!textField.getText().equals("")) {
						rs = publication.viewPublication(textField.getText());

						while (rs.next()) {
							pubName_box.setText(rs.getString("publication_name"));
							pubDate_box.setText(rs.getString("publication_date"));
							cost_box.setText("" + rs.getDouble("cost"));
							pubFreq_box.setText(rs.getString("publication_frequency"));
							genre_box.setText(rs.getString("genre"));

							if (rs.isLast()) {
								rs.beforeFirst();
								break;
							}

						}
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		button.setBounds(351, 68, 70, 22);
		frame.getContentPane().add(button);

		Button button_1 = new Button("Delete");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String r = "";
				if (textField.getText() == "") {
					JOptionPane.showMessageDialog(null, "No Customer Selected");
				} else {
					try {
						// Custom button text
						Object[] options = { "Yes, Delete", "No, Cancel", };
						int n = JOptionPane.showOptionDialog(frame,
								"Are you sure that you want to Delete the Publication ", "Warning",
								JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

						if (n == 0) {
							rs.beforeFirst();
							while (rs.next()) {
								r = publication.deletePublication(rs.getInt("id"));
								JOptionPane.showMessageDialog(null, r);
								if (r == "Publication has been removed.") {
									textField.setText("");
									pubName_box.setText("");
									pubDate_box.setText("");
									cost_box.setText("");
									pubFreq_box.setText("");
									genre_box.setText("");
									break;
								} else if (!r.equals("")) {
									break;
								}
								
							}
						} else if (n == 1) {

						}

					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		button_1.setBounds(243, 322, 86, 22);
		frame.getContentPane().add(button_1);
	}
}
